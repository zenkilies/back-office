<?php

namespace App\Policies;

use App\Models\ProductAttribute;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductAttributePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function view(User $user, ProductAttribute $productAttribute): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function create(User $user): \Illuminate\Auth\Access\Response
    {
        if ($user->isOwner()) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function update(User $user, ProductAttribute $productAttribute): \Illuminate\Auth\Access\Response
    {
        if ($user->isOwner()) {
            return $this->allow();
        }

        return $this->deny();
    }
}
