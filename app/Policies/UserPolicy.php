<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function view(User $user, User $model): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function update(User $user, User $model): \Illuminate\Auth\Access\Response
    {
        if ($user->isAdmin()) {
            return $this->allow();
        }

        return $this->deny();
    }
}
