<?php

namespace App\Policies;

use App\Models\ProductAttributeValue;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductAttributeValuePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function view(User $user): \Illuminate\Auth\Access\Response
    {
        return $this->allow();
    }

    public function create(User $user): \Illuminate\Auth\Access\Response
    {
        if ($user->isOwner()) {
            return $this->allow();
        }

        return $this->deny();
    }

    public function update(User $user, ProductAttributeValue $productAttributeValue): \Illuminate\Auth\Access\Response
    {
        if ($user->isOwner()) {
            return $this->allow();
        }

        return $this->deny();
    }
}
