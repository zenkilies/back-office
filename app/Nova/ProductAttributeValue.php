<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class ProductAttributeValue extends Resource
{
    public static $model = \App\Models\ProductAttributeValue::class;

    public static $title = 'id';

    public static $search = [
        'id', 'name'
    ];

    public static function redirectAfterCreate(NovaRequest $request, $resource): string
    {
        return sprintf('/resources/%s/%d', ProductAttribute::uriKey(), $resource->model()->productAttribute->id);
    }

    public static function redirectAfterUpdate(NovaRequest $request, $resource): string
    {
        return sprintf('/resources/%s/%d', ProductAttribute::uriKey(), $resource->model()->productAttribute->id);
    }

    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('Product Attribute', 'productAttribute', ProductAttribute::class),

            Text::make('Name')
                ->rules('required'),

            Boolean::make('Is Active')
                ->rules('required'),

            Number::make('Sequence')
                ->sortable()
                ->rules('required')
                ->default(0),
        ];
    }

    public function cards(NovaRequest $request)
    {
        return [];
    }

    public function filters(NovaRequest $request)
    {
        return [];
    }

    public function lenses(NovaRequest $request)
    {
        return [];
    }

    public function actions(NovaRequest $request)
    {
        return [];
    }
}
