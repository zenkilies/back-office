<?php

namespace App\Models;

use App\Services\SnowflakeService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;

class ProductAttribute extends Model
{
    use Actionable, HasFactory, SoftDeletes;

    protected $casts = [
        'id' => 'string',
    ];

    public $incrementing = false;

    static function boot()
    {
        parent::boot();

        static::creating(function (Model $record) {
            $record->id = resolve(SnowflakeService::class)->id();
        });
    }

    public function productAttributeValues(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ProductAttributeValue::class);
    }
}
