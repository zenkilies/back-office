<?php

namespace App\Models;

use App\Services\SnowflakeService;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'id' => 'string',
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (Model $record) {
            $record->id = resolve(SnowflakeService::class)->id();
        });
    }

    public function isAdmin(): bool
    {
        return $this->authority_level >= 4;
    }

    public function isOwner(): bool
    {
        return $this->authority_level >= 3;
    }

    public function isEditor(): bool
    {
        return $this->authority_level >= 2;
    }
}
