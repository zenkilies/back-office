<?php

namespace App\Models;

use App\Services\SnowflakeService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;

class ProductAttributeValue extends Model
{
    use Actionable, HasFactory, SoftDeletes;

    protected $casts = [
        'id' => 'string',
        'product_attribute_id' => 'string',
    ];

    public $incrementing = false;

    static function boot()
    {
        parent::boot();

        static::creating(function (Model $record) {
            $record->id = resolve(SnowflakeService::class)->id();
        });
    }

    public function productAttribute(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ProductAttribute::class);
    }
}
