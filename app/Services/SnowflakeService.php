<?php

namespace App\Services;

use Godruoyi\Snowflake\Snowflake;

class SnowflakeService
{
    private Snowflake $snowflake;

    public function __construct()
    {
        $this->snowflake = new Snowflake();
    }

    public function id(): string
    {
        return $this->snowflake->id();
    }
}
