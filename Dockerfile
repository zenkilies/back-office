FROM node:lts

WORKDIR /var/www/html

COPY package.json .

RUN npm install

COPY . .

RUN npm run production

FROM composer:2

ENV NOVA_USERNAME="zenkilies@gmail.com"
ENV NOVA_LICENSE_KEY="cboQQJp2wheZPsDgoyuBLPU6VWygaLovpEg32C0WOo7JNuUCe0"

WORKDIR /var/www/html

COPY . .

RUN composer config http-basic.nova.laravel.com $NOVA_USERNAME $NOVA_LICENSE_KEY
RUN composer install --prefer-dist --no-dev --optimize-autoloader --no-interaction

FROM php:8.0-apache-buster

ENV APACHE_DOCUMENT_ROOT="/var/www/html/public"

WORKDIR /var/www/html

RUN pecl install -o -f redis && rm -rf /tmp/pear
RUN docker-php-ext-configure opcache --enable-opcache
RUN docker-php-ext-enable redis
RUN docker-php-ext-install pdo pdo_mysql
RUN a2enmod rewrite

COPY --from=1 /var/www/html /var/www/html
COPY --from=0 /var/www/html/public /var/www/html/public

RUN chown -R www-data:www-data bootstrap/cache
RUN chown -R www-data:www-data storage

RUN chmod -R 775 bootstrap/cache
RUN chmod -R 775 storage

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
