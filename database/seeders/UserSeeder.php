<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function run()
    {
        foreach ($this->records as $record) {
            $record['created_at'] = Carbon::now();
            $record['updated_at'] = Carbon::now();

            DB::table('users')->upsert($record, 'id');
        }
    }

    private array $records = [
        [
            'id' => 112358132134558901,
            'name' => 'Nguyen Dinh Thai',
            'email' => 'nguyen.dinh@vtijs.com',
            'authority_level' => 4,
        ],
    ];
}
