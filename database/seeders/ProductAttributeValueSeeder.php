<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ProductAttributeValueSeeder extends Seeder
{
    public function run()
    {
        foreach ($this->records as $record) {
            $record['created_at'] = Carbon::now();
            $record['updated_at'] = Carbon::now();

            DB::table('product_attribute_values')->upsert($record, 'id');
        }
    }

    private array $records = [
        [
            'id' => 112358132134558901,
            'product_attribute_id' => 112358132134558901,
            'name' => 'Hot',
            'is_active' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558902,
            'product_attribute_id' => 112358132134558901,
            'name' => 'Ice',
            'is_active' => true,
            'sequence' => 1,
        ],
        [
            'id' => 112358132134558903,
            'product_attribute_id' => 112358132134558902,
            'name' => 'Size S',
            'is_active' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558904,
            'product_attribute_id' => 112358132134558902,
            'name' => 'Size M',
            'is_active' => true,
            'sequence' => 1,
        ],
        [
            'id' => 112358132134558905,
            'product_attribute_id' => 112358132134558902,
            'name' => 'Size L',
            'is_active' => true,
            'sequence' => 2,
        ],
        [
            'id' => 112358132134558906,
            'product_attribute_id' => 112358132134558903,
            'name' => 'Fresh Milk',
            'is_active' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558907,
            'product_attribute_id' => 112358132134558903,
            'name' => 'No Fresh Milk',
            'is_active' => true,
            'sequence' => 1,
        ],
        [
            'id' => 112358132134558908,
            'product_attribute_id' => 112358132134558904,
            'name' => 'Milk Foam',
            'is_active' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558909,
            'product_attribute_id' => 112358132134558904,
            'name' => 'Without Milk Foam',
            'is_active' => true,
            'sequence' => 1,
        ],
        [
            'id' => 112358132134558910,
            'product_attribute_id' => 112358132134558905,
            'name' => 'Milk',
            'is_active' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558911,
            'product_attribute_id' => 112358132134558905,
            'name' => 'No Milk',
            'is_active' => true,
            'sequence' => 1,
        ],
    ];
}
