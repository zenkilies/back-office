<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ProductAttributeSeeder extends Seeder
{
    public function run()
    {
        foreach ($this->records as $record) {
            $record['created_at'] = Carbon::now();
            $record['updated_at'] = Carbon::now();

            DB::table('product_attributes')->upsert($record, 'id');
        }
    }

    private array $records = [
        [
            'id' => 112358132134558901,
            'name' => 'Hot/Ice',
            'create_variant' => true,
            'sequence' => 0,
        ],
        [
            'id' => 112358132134558902,
            'name' => 'Size',
            'create_variant' => true,
            'sequence' => 1,
        ],
        [
            'id' => 112358132134558903,
            'name' => 'Fresh Milk/No Fresh Milk',
            'create_variant' => false,
            'sequence' => 2,
        ],
        [
            'id' => 112358132134558904,
            'name' => 'Milk Foam/Without Milk Foam',
            'create_variant' => false,
            'sequence' => 3,
        ],
        [
            'id' => 112358132134558905,
            'name' => 'Milk/No Milk',
            'create_variant' => false,
            'sequence' => 4,
        ],
    ];
}
